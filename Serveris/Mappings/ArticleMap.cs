﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Server.Entities;

namespace Server.Mappings
{
    public class ArticleMap : EntityBaseMap<Article>
    {
        public override void Configure(EntityTypeBuilder<Article> builder)
        {
            builder.ToTable("Articles");

            builder.Property(a => a.Content).IsRequired();
            builder.Property(a => a.Id).IsRequired();
            builder.Property(a => a.Title).HasMaxLength(50).IsRequired();
            builder.Property(a => a.UserId).IsRequired();

            builder.HasOne(a => a.User).WithMany(u => u.Articles).HasForeignKey(a => a.UserId).OnDelete(DeleteBehavior.Cascade);

            base.Configure(builder);
        }
    }
}
