﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Server.Entities;

namespace Server.Mappings
{
    public class UserMap : EntityBaseMap<User>
    {
        public override void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");

            builder.HasAlternateKey(u => u.Username);

            builder.Property(u => u.FirstName).HasMaxLength(25).IsRequired();
            builder.Property(u => u.Email).HasMaxLength(50).IsRequired();
            builder.Property(u => u.LastName).HasMaxLength(25).IsRequired();
            builder.Property(u => u.Username).HasMaxLength(25).IsRequired();

            builder.HasIndex(u => u.Email).IsUnique();

            base.Configure(builder);
        }
    }
}
