﻿using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Server.Authorization.Attributes;
using Server.Authorization.Roles;
using Server.Entities;
using Server.Services;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Controllers
{
    public class ArticlesController : CrudControllerBase<Article>
    {
        public ArticlesController(NewsDbContext newsDbContext) : base(newsDbContext) { }

        [AuthorizeRole(Role.Administrator)]
        public override Task<IActionResult> Delete([FromODataUri] int key)
        {
            return base.Delete(key);
        }

        public IQueryable<Comment> GetComments([FromODataUri] int key)
        {
            return _newsDbContext
                .Set<Article>()
                .AsNoTracking()
                .Where(a => a.Id == key)
                .SelectMany(a => a.Comments);
        }

        [ODataRoute("Articles({key})/Comments({relatedKey})")]
        public SingleResult<Comment> GetComment([FromODataUri] int key, [FromODataUri] int relatedKey)
        {
            return SingleResult.Create(
                _newsDbContext
                    .Set<Article>()
                    .AsNoTracking()
                    .Where(a => a.Id == key)
                    .SelectMany(a => a.Comments)
                    .Where(c => c.Id == relatedKey)
            );
        }

        public SingleResult<User> GetUser([FromODataUri] int key)
        {
            return SingleResult.Create(
                _newsDbContext
                    .Set<Article>()
                    .AsNoTracking()
                    .Where(a => a.Id == key)
                    .Select(a => a.User)
            );
        }

        [AuthorizeRole(Role.Editor)]
        public override async Task<IActionResult> Patch([FromODataUri] int key, [FromBody] Delta<Article> delta)
        {
            if (ValidateUser(key))
            {
                return await base.Patch(key, delta);
            }

            return Unauthorized();
        }

        [AuthorizeRole(Role.Editor)]
        public override Task<IActionResult> Post([FromBody] Article entity)
        {
            return base.Post(entity);
        }

        [AuthorizeRole(Role.Editor)]
        public override async Task<IActionResult> Put([FromODataUri] int key, [FromBody] Article entity)
        {
            if (ValidateUser(key))
            {
                return await base.Put(key, entity);
            }

            return Unauthorized();
        }

        private bool ValidateUser(int id)
        {
            return
                (
                    User.Identity.IsAuthenticated &&
                    User.Claims.Single(c => c.Type == "HighestRole").Value.ToRole() == Role.Administrator
                ) ||
                _newsDbContext.Set<Article>().Where(a => a.Id == id).Select(c => c.User).SingleOrDefault()?.Username == User.Identity.Name;
        }
    }
}
