﻿using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Server.Authorization.Attributes;
using Server.Authorization.Roles;
using Server.Entities;
using Server.Services;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Controllers
{
    public class CommentsController : CrudControllerBase<Comment>
    {
        public CommentsController(NewsDbContext newsDbContext) : base(newsDbContext) { }

        [AuthorizeRole(Role.Administrator)]
        public override async Task<IActionResult> Delete([FromODataUri] int key)
        {
            var comment = await _newsDbContext
                .Set<Comment>()
                .Include(c => c.Children)
                .SingleOrDefaultAsync(c => c.Id == key);

            if (comment is null)
            {
                return NotFound();
            }

            foreach (var child in comment.Children)
            {
                child.ParentId = null;
            }

            return await base.Delete(key);
        }

        public SingleResult<Article> GetArticle([FromODataUri] int key)
        {
            return SingleResult.Create(
                _newsDbContext
                    .Set<Comment>()
                    .AsNoTracking()
                    .Where(c => c.Id == key)
                    .Select(c => c.Article)
            );
        }

        [ODataRoute("Comments({key})/Children({relatedKey})")]
        public SingleResult<Comment> GetChild([FromODataUri] int key, [FromODataUri] int relatedKey)
        {
            return SingleResult.Create(_newsDbContext
                .Set<Comment>()
                .AsNoTracking()
                .Where(c => c.Id == key)
                .SelectMany(c => c.Children)
                .Where(c => c.Id == relatedKey)
            );
        }

        public IQueryable<Comment> GetChildren([FromODataUri] int key)
        {
            return _newsDbContext
                .Set<Comment>()
                .AsNoTracking()
                .Where(c => c.Id == key)
                .SelectMany(c => c.Children);
        }

        public SingleResult<Comment> GetParent([FromODataUri] int key)
        {
            return SingleResult.Create(_newsDbContext
                .Set<Comment>()
                .AsNoTracking()
                .Where(c => c.Id == key)
                .Select(c => c.Parent)
            );
        }

        public SingleResult<User> GetUser([FromODataUri] int key)
        {
            return SingleResult.Create(
                _newsDbContext
                    .Set<Comment>()
                    .AsNoTracking()
                    .Where(c => c.Id == key)
                    .Select(c => c.User)
            );
        }

        public override async Task<IActionResult> Patch([FromODataUri] int key, [FromBody] Delta<Comment> delta)
        {
            if (ValidateUser(key))
            {
                return await base.Patch(key, delta);
            }

            return Unauthorized();
        }

        [Authorize]
        public override Task<IActionResult> Post([FromBody] Comment entity)
        {
            return base.Post(entity);
        }

        public override async Task<IActionResult> Put([FromODataUri] int key, [FromBody] Comment entity)
        {
            if (ValidateUser(key))
            {
                return await base.Put(key, entity);
            }

            return Unauthorized();
        }

        private bool ValidateUser(int id)
        {
            return
                (
                    User.Identity.IsAuthenticated &&
                    User.Claims.Single(c => c.Type == "HighestRole").Value.ToRole() == Role.Administrator
                ) ||
                _newsDbContext.Set<Comment>().Where(c => c.Id == id).Select(c => c.User).SingleOrDefault()?.Username == User.Identity.Name;
        }
    }
}
