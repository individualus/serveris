﻿using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Server.Entities;
using Server.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Controllers
{
    [EnableQuery]
    public abstract class CrudControllerBase<T> : ODataController, IAsyncDisposable, IDisposable where T : class, IEntity
    {
        protected readonly NewsDbContext _newsDbContext;

        public CrudControllerBase(NewsDbContext newsDbContext)
        {
            _newsDbContext = newsDbContext;
        }

        ~CrudControllerBase()
        {
            Dispose();
        }

        public virtual async Task<IActionResult> Delete([FromODataUri] int key)
        {
            var entity = await _newsDbContext
                .Set<T>()
                .FindAsync(key);

            if (entity is null)
            {
                return NotFound();
            }

            _newsDbContext
                .Set<T>().
                Remove(entity);

            await _newsDbContext.SaveChangesAsync();

            return NoContent();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);

            DisposeAsync()
                .AsTask()
                .Wait();
        }

        public async ValueTask DisposeAsync()
        {
            await _newsDbContext.DisposeAsync();
        }

        public virtual IQueryable<T> Get()
        {
            return _newsDbContext
                .Set<T>()
                .AsNoTracking();
        }

        public virtual SingleResult<T> Get([FromODataUri] int key)
        {
            return SingleResult.Create(_newsDbContext
                .Set<T>()
                .AsNoTracking()
                .Where(w => w.Id == key)
            );
        }

        public virtual async Task<IActionResult> Patch([FromODataUri] int key, [FromBody] Delta<T> delta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var entity = await _newsDbContext
                .Set<T>()
                .FindAsync(key);

            if (entity is null)
            {
                return NotFound();
            }

            delta.Patch(entity);

            await _newsDbContext.SaveChangesAsync();

            return Updated(entity);
        }

        public virtual async Task<IActionResult> Post([FromBody] T entity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _newsDbContext
                .Set<T>()
                .Add(entity);

            await _newsDbContext.SaveChangesAsync();

            return Created(entity);
        }

        public virtual async Task<IActionResult> Put([FromODataUri] int key, [FromBody] T entity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (key != entity.Id)
            {
                return BadRequest();
            }

            var existing = await _newsDbContext
                .Set<T>()
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.Id == key);

            if (existing is null)
            {
                return NotFound();
            }

            _newsDbContext
                .Set<T>()
                .Update(entity);

            await _newsDbContext.SaveChangesAsync();

            return Updated(entity);
        }
    }
}
