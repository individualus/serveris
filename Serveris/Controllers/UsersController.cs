﻿using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Server.Authorization.Attributes;
using Server.Authorization.Roles;
using Server.Entities;
using Server.Services;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Controllers
{
    public class UsersController : CrudControllerBase<User>
    {
        public UsersController(NewsDbContext newsDbContext) : base(newsDbContext) { }

        [AuthorizeRole(Role.Administrator)]
        public override async Task<IActionResult> Delete([FromODataUri] int key)
        {
            var user = await _newsDbContext
                .Set<User>()
                .Include(u => u.Comments)
                .SingleOrDefaultAsync(u => u.Id == key);

            if (user is null)
            {
                return NotFound();
            }

            if (user.Comments.Count > 0)
            {
                _newsDbContext
                    .Set<Comment>()
                    .RemoveRange(user.Comments);
            }

            return await base.Delete(key);
        }

        [ODataRoute("Users(Username={username})")]
        public SingleResult<User> GetByUsername([FromODataUri] string username)
        {
            return SingleResult.Create(_newsDbContext
                .Set<User>()
                .AsNoTracking()
                .Where(u => u.Username == username)
            );
        }

        public IQueryable<Article> GetArticles([FromODataUri] int key)
        {
            return _newsDbContext
                .Set<User>()
                .AsNoTracking()
                .Where(u => u.Id == key)
                .SelectMany(u => u.Articles);
        }

        [ODataRoute("Users({key})/Articles({relatedKey})")]
        public SingleResult<Article> GetArticle([FromODataUri] int key, [FromODataUri] int relatedKey)
        {
            return SingleResult.Create(
                _newsDbContext
                    .Set<User>()
                    .AsNoTracking()
                    .Where(u => u.Id == key)
                    .SelectMany(u => u.Articles)
                    .Where(a => a.Id == relatedKey)
            );
        }

        [ODataRoute("Users({key})/Comments({relatedKey})")]
        public SingleResult<Comment> GetComment([FromODataUri] int key, [FromODataUri] int relatedKey)
        {
            return SingleResult.Create(
                _newsDbContext
                    .Set<User>()
                    .AsNoTracking()
                    .Where(u => u.Id == key)
                    .SelectMany(u => u.Comments)
                    .Where(c => c.Id == relatedKey)
            );
        }

        public IQueryable<Comment> GetComments([FromODataUri] int key)
        {
            return _newsDbContext
                .Set<User>()
                .AsNoTracking()
                .Where(u => u.Id == key)
                .SelectMany(u => u.Comments);
        }

        public override async Task<IActionResult> Patch([FromODataUri] int key, [FromBody] Delta<User> delta)
        {
            if (ValidateUser(key))
            {
                return await base.Patch(key, delta);
            }

            return Unauthorized();
        }

        public override async Task<IActionResult> Put([FromODataUri] int key, [FromBody] User entity)
        {
            if (ValidateUser(key))
            {
                return await base.Put(key, entity);
            }

            return Unauthorized();
        }

        private bool ValidateUser(int id)
        {
            return
                (
                    User.Identity.IsAuthenticated &&
                    User.Claims.Single(c => c.Type == "HighestRole").Value.ToRole() == Role.Administrator
                ) ||
                _newsDbContext.Set<User>().SingleOrDefault(u => u.Id == id)?.Username == User.Identity.Name;
        }
    }
}
