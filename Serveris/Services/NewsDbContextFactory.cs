﻿using Microsoft.EntityFrameworkCore.Design;

namespace Server.Services
{
    public class NewsDbContextFactory : IDesignTimeDbContextFactory<NewsDbContext>
    {
        public NewsDbContext CreateDbContext(string[] args)
        {
            return new NewsDbContext("Server=tcp:saitynas.database.windows.net,1433;Initial Catalog=Saitynas;Persist Security Info=False;User ID=SaitynasAdmin;Password=VeryCoolPassword123;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }
    }
}
