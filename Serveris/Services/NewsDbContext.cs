﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Server.Mappings;

namespace Server.Services
{
    public class NewsDbContext : DbContext
    {
        private readonly string _connectionString;

        public NewsDbContext(IConfiguration configuration)
        {
            _connectionString = configuration["ConnectionString"];
        }

        internal NewsDbContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseSqlServer(_connectionString);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new ArticleMap());
            builder.ApplyConfiguration(new CommentMap());
            builder.ApplyConfiguration(new UserMap());
        }
    }
}
