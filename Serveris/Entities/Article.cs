﻿using System.Collections.Generic;

namespace Server.Entities
{
    public class Article : IEntity
    {
        public Article()
        {
            Comments = new List<Comment>();
        }

        public virtual ICollection<Comment> Comments { get; set; }

        public string Content { get; set; }

        public int Id { get; set; }

        public string Title { get; set; }

        public virtual User User { get; set; }

        public int UserId { get; set; }
    }
}
