﻿using System.Collections.Generic;

namespace Server.Entities
{
    public class Comment : IEntity
    {
        public Comment() {
            Children = new List<Comment>();
        }

        public virtual Article Article { get; set; }

        public int ArticleId { get; set; }

        public virtual ICollection<Comment> Children { get; set; }

        public int Id { get; set; }

        public virtual Comment Parent { get; set; }

        public int? ParentId { get; set; }

        public string Text { get; set; }

        public virtual User User { get; set; }

        public int UserId { get; set; }
    }
}
