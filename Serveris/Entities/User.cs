﻿using System.Collections.Generic;

namespace Server.Entities
{
    public class User : IEntity
    {
        public User()
        {
            Articles = new List<Article>();
            Comments = new List<Comment>();
        }

        public virtual ICollection<Article> Articles { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public int Id { get; set; }

        public string LastName { get; set; }

        public string Username { get; set; }
    }
}
