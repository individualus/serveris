﻿using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Authorization.Roles
{
    public class RoleAuthorizationHandler : AuthorizationHandler<RoleRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, RoleRequirement requirement)
        {
            if
                (
                    context is null ||
                    context.User is null ||
                    requirement is null ||
                    context.HasFailed ||
                    context.HasSucceeded ||
                    !context.User.Identity.IsAuthenticated
                )
            {
                return Task.CompletedTask;
            }

            var role = context.User.Claims
                .Single(c => c.Type == "HighestRole").Value
                .ToRole();

            if (role >= requirement.Role)
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}
