﻿namespace Server.Authorization.Roles
{
    public static class RoleExtentions
    {
        public static Role ToRole(this string role)
        {
            return role switch
            {
                "Administrator" => Role.Administrator,
                "Editor" => Role.Editor,
                "User" => Role.User,
                _ => Role.Anonymous
            };
        }
    }
}
