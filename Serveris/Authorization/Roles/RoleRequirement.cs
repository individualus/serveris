﻿using Microsoft.AspNetCore.Authorization;

namespace Server.Authorization.Roles
{
    public class RoleRequirement : IAuthorizationRequirement
    {
        public Role Role { get; init; }
    }
}
