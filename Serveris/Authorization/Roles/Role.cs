﻿namespace Server.Authorization.Roles
{
    public enum Role
    {
        Anonymous = 0,
        User = 100,
        Editor = 200,
        Administrator = 300
    }
}
