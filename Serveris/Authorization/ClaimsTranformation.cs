﻿using Microsoft.AspNetCore.Authentication;
using Server.Authorization.Roles;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Server.Authorization
{
    public class ClaimsTranformation : IClaimsTransformation
    {
        public Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal)
        {
            var role = principal.Claims
                .Where(c => c.Type == ClaimTypes.Role)
                .Select(c => c.Value.ToRole())
                .Append(Role.Anonymous)
                .Max()
                .ToString();

            var identity = (ClaimsIdentity)principal.Identity;

            if (!identity.Claims.Any(c => c.Type == "HighestRole"))
            {
                identity.AddClaim(new Claim("HighestRole", role));
            }

            return Task.FromResult(principal);
        }
    }
}
