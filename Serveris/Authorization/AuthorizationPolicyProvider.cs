﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using Server.Authorization.Queries;
using Server.Authorization.Roles;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Authorization
{
    public class AuthorizationPolicyProvider : IAuthorizationPolicyProvider
    {
        public readonly DefaultAuthorizationPolicyProvider FallbackPolicyProvider;

        public AuthorizationPolicyProvider(IOptions<AuthorizationOptions> options)
        {
            FallbackPolicyProvider = new DefaultAuthorizationPolicyProvider(options);
        }

        public Task<AuthorizationPolicy> GetDefaultPolicyAsync()
        {
            return Task.FromResult(new AuthorizationPolicyBuilder("Bearer")
                .RequireAuthenticatedUser()
                .Build()
            );
        }

        public async Task<AuthorizationPolicy> GetFallbackPolicyAsync()
        {
            return await FallbackPolicyProvider.GetFallbackPolicyAsync();
        }

        public async Task<AuthorizationPolicy> GetPolicyAsync(string policyName)
        {
            if (string.IsNullOrWhiteSpace(policyName))
            {
                return await FallbackPolicyProvider.GetPolicyAsync(policyName);
            }

            var policyTokens = policyName.Split('&', StringSplitOptions.RemoveEmptyEntries);

            if (policyTokens.Length == 0)
            {
                return await FallbackPolicyProvider.GetPolicyAsync(policyName);
            }

            var policy = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme);

            foreach (var policyToken in policyTokens)
            {
                var token = policyToken.Split('@', StringSplitOptions.RemoveEmptyEntries);

                if (token.Length < 1)
                {
                    return await FallbackPolicyProvider.GetPolicyAsync(policyName);
                }

                switch (token[0])
                {
                    case "Query":
                        {
                            foreach (var query in token[1].Split('|', StringSplitOptions.RemoveEmptyEntries))
                            {
                                var values = query.Split(':', StringSplitOptions.RemoveEmptyEntries);

                                if (values.Length != 3)
                                {
                                    return await FallbackPolicyProvider.GetPolicyAsync(policyName);
                                }

                                policy.AddRequirements(new QueryRequirement
                                {
                                    Property = values[1],
                                    Query = values[0],
                                    Role = values[2].ToRole()
                                });
                            }

                            break;
                        }
                    case "Role":
                        {
                            policy.AddRequirements(new RoleRequirement
                            {
                                Role = token[1]
                                    .Split('|', StringSplitOptions.RemoveEmptyEntries)
                                    .Max(r => r.ToRole())
                            });

                            break;
                        }
                }
            }

            return policy.Build();
        }
    }
}
