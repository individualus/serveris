﻿using Server.Authorization.Roles;

namespace Server.Authorization.Queries
{
    public class QueryRequirement : RoleRequirement
    {
        public string Property { get; set; }

        public string Query { get; init; }
    }
}
