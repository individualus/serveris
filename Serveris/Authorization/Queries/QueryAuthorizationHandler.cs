﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Server.Authorization.Roles;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Authorization.Queries
{
    public class QueryAuthorizationHandler : AuthorizationHandler<QueryRequirement>
    {
        public readonly IHttpContextAccessor _httpContextAccessor;

        public QueryAuthorizationHandler(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, QueryRequirement requirement)
        {
            if
                (
                    context is null ||
                    context.User is null ||
                    requirement is null ||
                    context.HasFailed ||
                    context.HasSucceeded
                )
            {
                return Task.CompletedTask;
            }

            var role = context.User.Claims
                .Where(c => c.Type == "HighestRole")
                .Select(c => c.Value)
                .SingleOrDefault()
                .ToRole();

            if
                (
                    !(
                        _httpContextAccessor.HttpContext.Request.Query.TryGetValue(requirement.Query, out var query) &&
                        query.Any(q => q.Contains(requirement.Property)) &&
                        role < requirement.Role
                    )
                )
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}
