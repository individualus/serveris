﻿using Server.Authorization.Roles;

namespace Server.Authorization.Attributes
{
    public class AuthorizeQueryAttribute : Microsoft.AspNetCore.Authorization.AuthorizeAttribute
    {
        public AuthorizeQueryAttribute(string query, string property, Role role = Role.Anonymous)
        {
            Policy = $"Query@{query}:{property}:{role}";
        }
    }
}
