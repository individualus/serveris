﻿using Server.Authorization.Roles;

namespace Server.Authorization.Attributes
{
    public class AuthorizeRoleAttribute : Microsoft.AspNetCore.Authorization.AuthorizeAttribute
    {
        public AuthorizeRoleAttribute(Role role = Role.Anonymous)
        {
            Policy = $"Role@{role}";
        }
    }
}
