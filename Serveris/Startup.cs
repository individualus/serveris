using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNet.OData.Routing.Conventions;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OData;
using Microsoft.OData.Edm;
using Microsoft.OData.UriParser;
using Microsoft.OpenApi;
using Microsoft.OpenApi.Extensions;
using Microsoft.OpenApi.OData;
using Server.Authorization;
using Server.Authorization.Queries;
using Server.Authorization.Roles;
using Server.Entities;
using Server.Services;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Server
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<NewsDbContext>();

            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.Authority = Configuration["OAuth:Url"];
                    options.Events = new JwtBearerEvents
                    {
                        OnAuthenticationFailed = context =>
                        {
                            if (context.Exception is SecurityTokenExpiredException)
                            {
                                context.Response.Headers.Add("Token-Expired", "true");
                            }

                            return Task.CompletedTask;
                        }
                    };
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateAudience = true,
                        ValidateIssuer = true,
                        ValidateIssuerSigningKey = true,
                        ValidateLifetime = true,
                        ValidAudience = Configuration["OAuth:Audience"],
                        ValidIssuer = Configuration["OAuth:Url"]
                    };
                });

            services.AddCors(
                options => options.AddDefaultPolicy(
                    builder => builder
                        .WithOrigins(
                            Configuration
                                .GetSection("CORS")
                                .Get<string[]>()
                        )
                        .WithHeaders("Authorization", "Content-Type")
                        .AllowAnyMethod()
                )
            );

            services.AddAuthorization();

            services.AddHttpContextAccessor();

            services.AddSingleton<IClaimsTransformation, ClaimsTranformation>();
            services.AddSingleton<IAuthorizationPolicyProvider, AuthorizationPolicyProvider>();

            services.AddSingleton<IAuthorizationHandler, QueryAuthorizationHandler>();
            services.AddSingleton<IAuthorizationHandler, RoleAuthorizationHandler>();

            services.AddOData();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var model = GetEdmModel();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                using var writer = new StreamWriter("./openApiSpec.json", false);

                writer.Write(model
                    .ConvertToOpenApi()
                    .SerializeAsJson(OpenApiSpecVersion.OpenApi3_0)
                );
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoint =>
            {
                endpoint
                    .Count()
                    .Expand()
                    .Filter()
                    .OrderBy()
                    .Select();

                endpoint.MapODataRoute(
                    "odata",
                    "",
                    builder => builder
                        .AddDefaultODataServices()
                        .AddService(
                            Microsoft.OData.ServiceLifetime.Singleton,
                            sp => model
                        )
                        .AddService<IEnumerable<IODataRoutingConvention>>(
                            Microsoft.OData.ServiceLifetime.Singleton,
                            sp => ODataRoutingConventions.CreateDefaultWithAttributeRouting("odata", endpoint.ServiceProvider)
                        )
                        .AddService<ODataUriResolver>(
                            Microsoft.OData.ServiceLifetime.Singleton,
                            sp => new AlternateKeysODataUriResolver(model) { EnableCaseInsensitive = true }
                        )
                );
            });
        }

        private static IEdmModel GetEdmModel()
        {
            var builder = new ODataConventionModelBuilder();

            builder.EntitySet<Article>("Articles");
            builder.EntitySet<Comment>("Comments");
            builder.EntitySet<User>("Users");

            var model = (EdmModel)builder.GetEdmModel();

            var user = model
                .FindDeclaredEntitySet("Users")
                .EntityType();

            model.AddAlternateKeyAnnotation(user, new Dictionary<string, IEdmProperty> { { "Username", user.FindProperty("Username") } });

            return model;
        }
    }
}
